import {Picker, StyleSheet} from 'react-native'
import React from 'react';

export default function PickerCustom(props){
    return (
        <Picker
            style={styles.picker}
            selectedValue={props.id}
            onValueChange={props.setId}>
            <Picker.Item 
                key={-1}
                label="Selecione"
                value={-1}/>
            {
                props.lista.map((item) => {
                    return <Picker.Item 
                            key={item.id}
                            label={item.descricao ? item.descricao : item.nome}
                            value={item.id}/>
                })
            }
        </Picker>
    );
}

const styles = StyleSheet.create({
    picker:{
        borderWidth: 1,
        justifyContent: 'center',
        marginBottom: 20,
        borderRadius: 5
    }
});