import React from 'react';
import {DatePickerIOS, View, StyleSheet, DatePickerAndroid} from 'react-native';
import { Platform } from '@unimodules/core';

export default function DatePickerCustom(props){
  setDateAndroid = async () => {
    try {
      const {
        action, year, month, day
      } = await DatePickerAndroid.open({
        date: new Date(),
        minDate: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        props.setState(`${year}-${month + 1}-${day}`);
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  };
  if(Platform.OS === 'ios'){
    return (
      <DatePickerIOS
        placeholder={props.placeholder}
        placeholderTextColor="#999"
        date={props.date}
        onDateChange={props.setDate}
      />
    );
  }else{
    return (
      <View style={MainStyles.AndroidDatePickerWrap}>
      <TouchableOpacity onPress={() => this.setDateAndroid()}>
        <View style={MainStyles.HistoryTime}>
          <Icon name="ios-calendar" size={25} color="rgb(49, 49, 49)" />
          <Text style={[MainStyles.HistoryTimeText, { fontSize: 16 }]}>
            {androidDate}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});