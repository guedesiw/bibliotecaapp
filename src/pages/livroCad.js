import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, KeyboardAvoidingView, Alert, Picker} from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { Platform } from '@unimodules/core';
import api from '../services/api';
import PickerCustom from '../components/pickerCustom';
import DatePickerCustom from '../components/datePickerCustom';

export default function LivroCad(){

    const [nome          , setNome          ] = useState('');
    const [volume        , setVolume        ] = useState('');
    const [dataPublicacao, setDataPublicacao] = useState(Date.now);
    const [valor         , setValor         ] = useState('');
    const [generos       , setGeneros       ] = useState([]);
    const [idGenero      , setIdGenero      ] = useState('');
    const [idEditora     , setIdEditora     ] = useState('1');
    const [idAutor       , setIdAutor       ] = useState('1');
    
    async function carregarGeneros(){//Para usar o await a função deve ser assincrona.
        try{
            const response = await api.get('/generos');//O get é assincrono.
            setGeneros(response.data);//.data pega o JSON.
        } catch (error) {}
        //console.log(response.data);
    }
    useEffect(()=>{
        carregarGeneros();
    },[])
    
    async function handleSubmit(){
        try {
            const response = await api.post('/livros', 
            {
                nome,volume,dataPublicacao,valor,genero: {id:idGenero}, editora: {id:idEditora}, autor: {id:idAutor}
            });
            setNome('');
            setVolume('');
            setDataPublicacao('');
            setValor('');
            setIdGenero('');
        } catch (error) {
            console.log(error);
            Alert.alert('Erro ao realizar a operação, tente novamente mais tarde!');
        }
    }

    return(
        <KeyboardAvoidingView
            enabled={Platform.OS == 'ios'}
            behavior="padding"
            style={styles.container}>
                <Text> Cadastro de Livro </Text>
                <View style={styles.form}>
                    <TextInput 
                        style={styles.input}
                        placeholder="Nome do livro"
                        placeholderTextColor="#999"
                        value={nome}
                        onChangeText={setNome}/>
                    <TextInput 
                        style={styles.input}
                        placeholder="Volume do livro"
                        placeholderTextColor="#999"
                        value={volume}
                        onChangeText={setVolume}/>
                    <TextInput 
                        style={styles.input}
                        placeholder="Valor"
                        placeholderTextColor="#999"
                        value={valor}
                        onChangeText={setValor}/>
                    <DatePickerCustom 
                        date={dataPublicacao}
                        setDate={setDataPublicacao}/>
                    <PickerCustom
                            id={idGenero}
                            setId={setIdGenero}
                            lista={generos}/>
                    
                    <TouchableOpacity
                        style={styles.botao}
                        onPress={handleSubmit}>
                            <Text style={styles.botaoTexto}> Salvar </Text>
                    </TouchableOpacity>
                </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    titulo : {
        fontSize: 20
    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        marginTop: 30
    },
    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8
    },
    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 18,
        color: '#444',
        marginBottom: 20,
        borderRadius: 5
    },
    botao: {
        height: 42,
        backgroundColor: '#00ced1',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2
    },
    botaoTexto: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16
    },
    datePiker:{
        flex: 1,
        justifyContent: 'center'
    },
});