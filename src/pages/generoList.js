import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native'
import api from '../services/api';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function GeneroList(){

    const [generos, setGeneros] = useState({});//State armazena informações.

    async function carregarGeneros(){//Para usar o await a função deve ser assincrona.
        const response = await api.get('/generos');//O get é assincrono.
        setGeneros(response.data);//.data pega o JSON.
        //console.log(response.data);
    }

    carregarGeneros();

    return(
        <View style={styles.container}>
        <Text style={styles.titulo}>Lista de Genero</Text>
        <FlatList data={generos} 
          style={styles.lista}
          keyExtractor={genero => `${genero.id}`}
          renderItem={({item}) => (
              <View style={styles.container}>
                <View style={styles.card}>
                  <Text style={styles.label} >Id: {item.id}</Text>
                  <Text style={styles.label}>Descrição: {item.descricao}</Text>
                  <TouchableOpacity onPress={async () => {
                      const id = item.id;
                      await api.delete(`/generos/${id}`);
                  }}>
                      <Text style={styles.botaoTexto}>Excluir</Text>
                  </TouchableOpacity>
                </View>
              </View>
          ) }
         />
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        marginTop: 10,
        backgroundColor: '#0000FF',
        flex: 1,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    lista:{
        paddingHorizontal: 20
    },
    titulo:{
       fontSize: 18,
       marginTop: 30,
       color: '#FFF',
       fontWeight: 'bold',
       textAlign: 'center'
    },
    label:{
        fontWeight: 'bold',
        color: '#444'
    },
    card:{
        backgroundColor: '#FFF',
        borderRadius: 5,// Consegue arredondar o componente.
        padding: 10
    },
    botaoTexto:{
        color: '#F05A5B',
        fontWeight: 'bold',

    }
})