import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import GeneroCad from './pages/generoCad';
import GeneroList from './pages/generoList';
import LivroList from './pages/livroList';
import LivroCad from './pages/livroCad';

const Routes = createAppContainer(
    createDrawerNavigator({
        CadastroGenero: {
            screen: GeneroCad,
            navigationOptions: {
                drawerLabel: "Cadastro de genero"
            }
        },
        ListaGenero: {
            screen: GeneroList,
            navigationOptions: {
                drawerLabel: "Lista de genero"
            }
        },
        CadastroLivros: {
            screen: LivroCad,
            navigationOptions: {
                drawerLabel: "Cadastro de livro"
            }
        },
        ListaLivros: {
            screen: LivroList,
            navigationOptions: {
                drawerLabel: "Lista de livros"
            }
        }
    })
);
export default Routes;